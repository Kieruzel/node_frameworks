const Koa = require('koa');
const Router = require('koa-router');
const app = new Koa();
const router = new Router();
const port = 3000;

// Route that responds with JSON
router.get('/json', async ctx => {
  ctx.type = 'application/json';
  ctx.body = { message: 'This is a JSON response' };
});

// Route that responds with HTML
router.get('/html', async ctx => {
  ctx.type = 'text/html';
  ctx.body = '<h1>This is an HTML response</h1>';
});

// Route that responds with plain text
router.get('/text', async ctx => {
  ctx.type = 'text/plain';
  ctx.body = 'This is a plain text response';
});

// Route that responds with an image
router.get('/image', async ctx => {
  ctx.type = 'image/png';
  ctx.body = Buffer.from('...image data...');
});

// Route that responds with XML
router.get('/xml', async ctx => {
  ctx.type = 'application/xml';
  ctx.body = '<message>This is an XML response</message>';
});

app.use(router.routes()).use(router.allowedMethods());

// Logger Middleware
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
});

// Authentication Middleware)
app.use(async (ctx, next) => {
  const token = ctx.headers.authorization;
  if (!token) {
    ctx.status = 401;
    ctx.body = 'Unauthorized';
    return;
  }
  await next();
});

// Response Middleware
app.use(async ctx => {
  ctx.body = 'Hello, Koa!';
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});