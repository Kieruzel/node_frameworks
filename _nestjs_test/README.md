## NEST.JS ## 

1. Install nest.js CLI ```npm install -g @nestjs/cli```
2. Create three resources User, Post, Thumb
3. Add database layer to resources 
4. Create validation and dto's for resources
5. Seed some data with faker library

Additinaly: 
6. Add multer to store files
7. Add passport for authentication

Tip: Use ```nest g resource name``` to generate resource
Tip: Use typeorm to create entity – look at package.json to see how to run migrations