import { Body, Controller, Get, Post, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { Response } from 'express';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('hello') 
  sayHelloInJson(@Res() res : Response): object {
    return this.appService.sayHelloInJson(res)
  }

  @Post('hello')
  sayHelloInJsonPost(@Body() data : any): object {
    return this.appService.sayHelloInJsonPost(data)
  }
}
