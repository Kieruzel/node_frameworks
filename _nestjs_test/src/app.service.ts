import { Injectable } from '@nestjs/common';
import { Response } from 'express';

@Injectable()
export class AppService {

  getHello(): string {
    return 'Hello World!';
  }

  sayHelloInJson(res: Response): object {
    console.log(res)
    return res.json({ message: 'Hello World!' })  
  }

  sayHelloInJsonPost(data: any): object {
    return {date: new Date(), ...data}
  }
}
