import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Zus {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;
}
