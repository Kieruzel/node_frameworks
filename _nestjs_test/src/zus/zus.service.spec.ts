import { Test, TestingModule } from '@nestjs/testing';
import { ZusService } from './zus.service';

describe('ZusService', () => {
  let service: ZusService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ZusService],
    }).compile();

    service = module.get<ZusService>(ZusService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
