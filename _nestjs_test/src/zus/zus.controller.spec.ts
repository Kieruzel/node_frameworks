import { Test, TestingModule } from '@nestjs/testing';
import { ZusController } from './zus.controller';
import { ZusService } from './zus.service';

describe('ZusController', () => {
  let controller: ZusController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ZusController],
      providers: [ZusService],
    }).compile();

    controller = module.get<ZusController>(ZusController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
