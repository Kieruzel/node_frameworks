import { Module } from '@nestjs/common';
import { ZusService } from './zus.service';
import { ZusController } from './zus.controller';
import { Type } from 'class-transformer';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Zus } from 'src/zus/entities/zus.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Zus])],
  controllers: [ZusController],
  providers: [ZusService]
})
export class ZusModule {}
