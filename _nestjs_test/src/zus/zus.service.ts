import { Injectable } from '@nestjs/common';
import { CreateZusDto } from './dto/create-zus.dto';
import { UpdateZusDto } from './dto/update-zus.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Zus } from 'src/zus/entities/zus.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class ZusService {
  constructor(
    @InjectRepository(Zus) private readonly zusRepository: Repository<Zus>,
    private dataSource: DataSource,
  ) {}

  async create(createZusDto: CreateZusDto) {
    try {

      // const zus = this.zusRepository.create(createZusDto);
      return await this.zusRepository.save(createZusDto);



    } catch (error) {}
  }

  findAll() {
    return `This action returns all zus`;
  }

  findOne(id: number) {
    return `This action returns a #${id} zus`;
  }

  update(id: number, updateZusDto: UpdateZusDto) {
    return `This action updates a #${id} zus`;
  }

  remove(id: number) {
    return `This action removes a #${id} zus`;
  }
}
