import { PartialType } from '@nestjs/mapped-types';
import { CreateZusDto } from './create-zus.dto';

export class UpdateZusDto extends PartialType(CreateZusDto) {}
