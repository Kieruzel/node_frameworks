import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class CreateZusDto {
  @IsString()
  @IsOptional()
  @ApiProperty()
  name: string;
}
